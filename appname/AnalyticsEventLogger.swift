//
//  AnalyticsEventLogger.swift
//  appname
//
//  2016 iAchieved.it.
//  This is free and unencumbered software released into the public domain.
//

import Foundation

protocol AnalyticsEventLogger {
  
  func initialize()
  func logEvent(name:String)
  func logEvent(name:String, data:AnyObject)
  
}