//
//  AmplitudeEventLogger.swift
//  appname
//
//  2016 iAchieved.it.
//  This is free and unencumbered software released into the public domain.
//

import Foundation

class AmplitudeEventLogger : AnalyticsEventLogger {
  
  let applicationKey = valueForAPIKey(named:"AmplitudeDevelopmentApplicationKey")
  
  func initialize() {
    Amplitude.instance().initializeApiKey(applicationKey)
  }
  
  func logEvent(name:String) {
    Amplitude.instance().logEvent(name)
  }
  
  func logEvent(name:String, data:AnyObject) {
    Amplitude.instance().logEvent(name, withEventProperties: data as! [NSObject : AnyObject])
  }

}