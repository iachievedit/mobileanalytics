//
//  EventConstants.swift
//  appname
//
//  2016 iAchieved.it.
//  This is free and unencumbered software released into the public domain.
//

import Foundation

let AnalyticsStartEvent   = "StartEvent"
let AnalyticsLikeEvent    = "LikeEvent"
let AnalyticsDislikeEvent = "DislikeEvent"
let AnalyticsShareEvent   = "ShareEvent"
let AnalyticsDonateEvent  = "DonateEvent"

let AnalyticsWatchStartEvent   = "WatchStartEvent"
let AnalyticsWatchLikeEvent    = "WatchLikeEvent"
let AnalyticsWatchDislikeEvent = "WatchDislikeEvent"
let AnalyticsWatchShareEvent   = "WatchShareEvent"