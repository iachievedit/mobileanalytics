//
//  CleverTapEventLogger.swift
//  appname
//
//  2016 iAchieved.it.
//  This is free and unencumbered software released into the public domain.
//

import Foundation

class CleverTapEventLogger : AnalyticsEventLogger {
  
  let accountID    = valueForAPIKey(named:"CleverTapDevelopmentAccountID")
  let accountToken = valueForAPIKey(named:"CleverTapDevelopmentAccountToken")
  
  func initialize() {
    CleverTap.changeCredentials(withAccountID: accountID, andToken: accountToken)
  }
  
  func logEvent(name:String) {
    CleverTap.sharedInstance().recordEvent(name)
  }
  
  func logEvent(name:String, data:AnyObject) {
    CleverTap.sharedInstance().recordEvent(name, withProps: data as! [NSObject:AnyObject])
  }
  
}