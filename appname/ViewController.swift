//
//  ViewController.swift
//  appname
//
//  2016 iAchieved.it.
//  This is free and unencumbered software released into the public domain.
//

import UIKit

class ViewController: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  @IBAction func likePressed(sender: AnyObject) {
    let appDelegate = UIApplication.shared().delegate as! AppDelegate
    appDelegate.eventLogger.logEvent(AnalyticsLikeEvent)
  }
  
  @IBAction func dislikePressed(sender: AnyObject) {
    let appDelegate = UIApplication.shared().delegate as! AppDelegate
    appDelegate.eventLogger.logEvent(AnalyticsDislikeEvent)
  }
  
  @IBAction func sharePressed(sender: AnyObject) {
    let appDelegate = UIApplication.shared().delegate as! AppDelegate
    appDelegate.eventLogger.logEvent(AnalyticsShareEvent)
  }
  
  @IBOutlet weak var donateAmountTextField: UITextField!
  @IBAction func donatePressed(sender: AnyObject) {
    if let donateAmount = donateAmountTextField.text {
      let appDelegate = UIApplication.shared().delegate as! AppDelegate
      appDelegate.eventLogger.logEvent(AnalyticsDonateEvent, data: ["amount":donateAmount])
    }
  }

}

