//
//  ApiKeys.swift
//  appname
//
//  2016 iAchieved.it.
//  This is free and unencumbered software released into the public domain.
//

import Foundation

func valueForAPIKey(named keyname:String) -> String? {
  // Credit to the original source for this technique at
  // http://blog.lazerwalker.com/blog/2014/05/14/handling-private-api-keys-in-open-source-ios-apps
  if let filePath = NSBundle.main().path(forResource: "ApiKeys", ofType: "plist"),
    let plist = NSDictionary(contentsOfFile:filePath) {
     let value = plist.object(forKey: keyname) as! String
     return value
  } else {
    return nil
  }
}

